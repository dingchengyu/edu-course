package com.lagou.course.remote;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.course.api.dto.LessonDTO;
import com.lagou.course.entity.Lesson;
import com.lagou.course.service.ILessonService;
import com.lagou.message.api.MessageRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Author:   mkp
 * Date:     2020/6/21 11:17
 * Description:
 */
@Slf4j
@RestController
@RequestMapping("/remote/lesson")
public class LessonService {
    @Autowired
    private ILessonService lessonService;
    @Autowired
    private MessageRemoteService messageRemoteService;

    @PostMapping(value = "/saveOrUpdate", consumes = "application/json")
    public boolean saveOrUpdate(@RequestBody LessonDTO lessonDTO) {
        Lesson lesson = new Lesson();
        BeanUtils.copyProperties(lessonDTO,lesson);
        if(lesson.getId() == null){
            lesson.setCreateTime(new Date());
        }else{
            messageRemoteService.putMessage(lesson.getId());
        }
        lesson.setUpdateTime(new Date());
        return lessonService.saveOrUpdate(lesson);
    }

    @GetMapping(value = "/getById")
    public LessonDTO getById(Integer lessonId) {
        Lesson lesson = lessonService.getById(lessonId);
        if(lesson == null){
            return null;
        }
        LessonDTO lessonDTO = new LessonDTO();
        BeanUtils.copyProperties(lesson,lessonDTO);
        return lessonDTO;
    }

    @GetMapping(value = "/getByIds")
    public Map<Integer, String> getByIds(List<Integer> lessonIds) {

        QueryWrapper<Lesson> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id",lessonIds);
        List<Lesson> lessons = lessonService.list(queryWrapper);
        if(CollectionUtils.isEmpty(lessons)){
            return Collections.EMPTY_MAP;
        }
        return lessons.stream().collect(Collectors.toMap(Lesson::getCourseId,Lesson::getTheme));
    }
}
