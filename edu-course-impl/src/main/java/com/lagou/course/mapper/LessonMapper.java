package com.lagou.course.mapper;

import com.lagou.course.entity.Lesson;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程节内容 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface LessonMapper extends BaseMapper<Lesson> {

}
