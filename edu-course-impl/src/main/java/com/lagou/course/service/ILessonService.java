package com.lagou.course.service;

import com.lagou.course.entity.Lesson;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程节内容 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface ILessonService extends IService<Lesson> {

    Integer getReleaseCourse(Integer courseId);

    List<Lesson> getBySectionId(Integer sectionId);
}
