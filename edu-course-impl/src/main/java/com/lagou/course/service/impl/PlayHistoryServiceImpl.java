package com.lagou.course.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.course.api.dto.PlayHistoryDTO;
import com.lagou.course.entity.Lesson;
import com.lagou.course.entity.PlayHistory;
import com.lagou.course.mapper.PlayHistoryMapper;
import com.lagou.course.service.ILessonService;
import com.lagou.course.service.IPlayHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@Slf4j
@Service
public class PlayHistoryServiceImpl extends ServiceImpl<PlayHistoryMapper, PlayHistory> implements IPlayHistoryService {

    @Autowired
    private ILessonService lessonService;
    private static final ExecutorService executorService = Executors.newFixedThreadPool(10);

    @Override
    public PlayHistory getByUserIdAndCourseId(Integer userId, Integer courseId) {
        QueryWrapper<PlayHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("lesson_id", courseId);
        queryWrapper.orderByDesc("update_time");
        List<PlayHistory> coursePlayHistories = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(coursePlayHistories)) {
            return null;
        }
        return coursePlayHistories.get(0);
    }

    @Override
    public void saveCourseHistoryNode(PlayHistoryDTO playHistoryDTO) {
        checkParams(playHistoryDTO);
        //待更新或保存的历史记录信息
        executorService.execute(new PlayHistoryTask(playHistoryDTO));
    }

    //开启一个保存播放记录的任务
    public class PlayHistoryTask implements Runnable {
        private PlayHistoryDTO playHistory;

        public PlayHistoryTask(PlayHistoryDTO playHistory) {
            this.playHistory = playHistory;
        }

        @Override
        public void run() {
            if (playHistory == null) {
                return;
            }
            //待更新或保存的历史记录信息
            recordPlayHistory(playHistory);
        }
    }

    /**
     * 保存用户播放记录
     *
     * @param playHistory
     */
    private void recordPlayHistory(PlayHistoryDTO playHistory) {
        //先检查上报课程信息的合法性
        if (!checkValidCourse(playHistory)) {
            return;
        }
        PlayHistory historyRecord = new PlayHistory();
        historyRecord.setHistoryNode(playHistory.getHistoryNode());

        Integer userId = playHistory.getUserId();
        Integer lessonId = playHistory.getLessonId();
        Integer node = playHistory.getHistoryNode();


        QueryWrapper<PlayHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        queryWrapper.eq("lesson_id", lessonId);
        queryWrapper.orderByDesc("update_time");
        List<PlayHistory> playHistories = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(playHistories)) {
            BeanUtils.copyProperties(playHistory, historyRecord);
            //兼容老逻辑，
            if (historyRecord.getSectionId() == null) {
                Lesson lesson = lessonService.getById(lessonId);
                historyRecord.setSectionId(lesson.getSectionId());
            }
            historyRecord.setHistoryHighestNode(node);
            historyRecord.setCreateTime(new Date());
            historyRecord.setUpdateTime(new Date());
            baseMapper.insert(historyRecord);
            return;
        }
        PlayHistory coursePlayHistory = playHistories.get(0);
        Integer historyId = coursePlayHistory.getId();
        Integer historyHighestNode = coursePlayHistory.getHistoryHighestNode();
        Integer oldHistoryNode = coursePlayHistory.getHistoryNode();
        updatePlayHistory(historyId, node, historyRecord, historyHighestNode, oldHistoryNode);
    }

    /**
     * 更新课程播放记录
     *
     * @param historyId             历史记录主键ID
     * @param node                  当前播放点
     * @param historyRecord         待更新的历史记录信息
     * @param oldHistoryHighestNode 之前的最高播放记录
     */
    private void updatePlayHistory(Integer historyId, Integer node, PlayHistory historyRecord, Integer oldHistoryHighestNode, Integer oldHistoryNode) {

        //兼容加history_highest_node字段前的数据
        if (oldHistoryHighestNode == null) {
            oldHistoryHighestNode = oldHistoryNode;
        }

        //设置最高播放记录
        if (oldHistoryHighestNode < node) {
            historyRecord.setHistoryHighestNode(node);
        } else {
            historyRecord.setHistoryHighestNode(oldHistoryHighestNode);
        }

        historyRecord.setUpdateTime(new Date());
        historyRecord.setId(historyId);
        try {
            baseMapper.updateById(historyRecord);
        } catch (Exception e) {
            log.error("更新播放记录失败,coursePlayHistory={}", historyRecord, e);
        }
    }

    //检查有效课程
    private boolean checkValidCourse(PlayHistoryDTO playHistory) {
        Lesson lesson = lessonService.getById(playHistory.getLessonId());
        if (lesson == null) {
            log.info("lessonId:{}对应的课时不存在,playHistory:{}", playHistory.getLessonId(), JSON.toJSONString(playHistory));
            return false;
        }

        //判断lessonId对应的课程ID和章节ID是否正确
        if (Objects.equals(lesson.getCourseId(), playHistory.getCourseId())
                && Objects.equals(lesson.getSectionId(), playHistory.getSectionId())) {
            return true;
        }

        log.info("上报的课程信息不匹配，上报监控,playHistory:{}", JSON.toJSONString(playHistory));
        return false;
    }

    /**
     * 检查参数是否有效
     *
     * @param playHistory
     * @return
     */
    private boolean checkParams(PlayHistoryDTO playHistory) {
        if (playHistory == null) {
            log.info("playHistory为空");
            return false;
        }

        if (playHistory.getUserId() == null) {
            log.info("playHistory.getLagouUserId为空");
            return false;
        }

        if (playHistory.getCourseId() == null) {
            log.info("playHistory.getCourseId为空");
            return false;
        }

        if (playHistory.getLessonId() == null) {
            log.info("playHistory.getLessonId为空");
            return false;
        }

        if (playHistory.getHistoryNode() == null) {
            log.info("playHistory.getHistoryNode为空");
            return false;
        }

        return true;
    }
}
