package com.lagou.course.service;

import com.lagou.course.entity.Media;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface IMediaService extends IService<Media> {

}
