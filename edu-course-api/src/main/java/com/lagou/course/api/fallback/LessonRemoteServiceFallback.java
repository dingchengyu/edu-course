package com.lagou.course.api.fallback;

import com.lagou.course.api.LessonRemoteService;
import com.lagou.course.api.dto.LessonDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:46
 */

@Slf4j
public class LessonRemoteServiceFallback implements LessonRemoteService {
    @Override
    public boolean saveOrUpdate(LessonDTO lessonDTO) {
        log.info("LessonRemoteService.saveOrUpdate ");
        return false;
    }

    @Override
    public LessonDTO getById(Integer lessonId) {
        log.info("LessonRemoteService.getById ");
        return null;
    }

    @Override
    public Map<Integer, String> getByIds(List<Integer> lessonIds) {
        log.info("LessonRemoteService.getByIds ");
        return null;
    }
}
