package com.lagou.course.api.fallback;

import com.lagou.course.api.TeacherRemoteService;
import com.lagou.course.api.dto.TeacherDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:49
 */
@Component
@Slf4j
public class TeacherRemoteServiceFallback implements TeacherRemoteService {
    @Override
    public TeacherDTO getTeacherByCourseId(Integer courseId) {
        log.info("TeacherRemoteService.getTeacherByCourseId");
        return null;
    }
}
