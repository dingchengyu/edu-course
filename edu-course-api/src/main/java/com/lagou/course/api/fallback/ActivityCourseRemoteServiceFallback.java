package com.lagou.course.api.fallback;

import com.lagou.common.result.ResponseDTO;
import com.lagou.course.api.ActivityCourseRemoteService;
import com.lagou.course.api.dto.ActivityCourseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:23
 */

@Component
@Slf4j
public class ActivityCourseRemoteServiceFallback implements ActivityCourseRemoteService {

    @Override
    public ResponseDTO<?> saveActivityCourse(ActivityCourseDTO reqDTO) {
        log.info("ActivityCourseRemoteService.saveActivityCourse ");
        return null;
    }

    @Override
    public ResponseDTO<?> updateActivityCourseStatus(ActivityCourseDTO reqDTO) {
        log.info("ActivityCourseRemoteService.updateActivityCourseStatus");
        return null;
    }

    @Override
    public ResponseDTO<ActivityCourseDTO> getById(Integer id) {
        log.info("ActivityCourseRemoteService.getById");
        return null;
    }

    @Override
    public ResponseDTO<ActivityCourseDTO> getByCourseId(Integer courseId) {
        log.info("ActivityCourseRemoteService.getByCourseId");
        return null;
    }

    @Override
    public ResponseDTO<?> updateActivityCourseStock(Integer courseId, String orderNo) {
        log.info("ActivityCourseRemoteService.updateActivityCourseStock");
        return null;
    }

}
